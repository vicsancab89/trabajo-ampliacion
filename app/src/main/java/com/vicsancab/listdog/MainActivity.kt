package com.vicsancab.listdog

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle


import android.widget.Toast
import androidx.appcompat.widget.SearchView

import androidx.recyclerview.widget.LinearLayoutManager
import com.vicsancab.listdog.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

//Searchview implementa los listener
class MainActivity : AppCompatActivity(), SearchView.OnQueryTextListener{

    //Implementamos el View Binding
    private lateinit var binding: ActivityMainBinding
    //Lateinit inicializa mas tarde
    private lateinit var adapter: DogAdapter
    //Inicia como lista vacía muta la lista tras la búsqueda del usuario
    private val dogImages = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Implementa el binding
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //Llamanda al método initRecyclerview
        initRecyclerView()
        binding.svDogs.setOnQueryTextListener(this)
    }
    //Esta función retornará una instancia de retrofit
    private fun getRetrofit(): Retrofit {
        //La configuramos con la función builder
        return Retrofit.Builder()
                //Parte no mutable de la url siempre debe de acabar con /
            .baseUrl("https://dog.ceo/api/breed/")
                //Implementa la librería que recupera el json y lo convierte en DogsResponse
            .addConverterFactory(GsonConverterFactory.create())
                //Finalizamos con build
            .build()

    }
    //Corrutina para conexión con la api al buscar la raza del perro
    private fun searchByName(query:String){
        CoroutineScope(Dispatchers.IO).launch {
            //Creamos una variable que a traves de la función create recibe la interfaz que queramos.
            //en query pasamos la raza como string junto a /images
            //la variable contiene un dogsresponse
            val call = getRetrofit().create(APIService::class.java).getDogsByBreeds("$query/images")
            val puppies = call.body()
            runOnUiThread {
                //si la variable call se ha completado con éxito
                if (call.isSuccessful) {
                    //muestra Recyclerview
                    //Creamos la variable images puede ser null
                    val images = puppies?.images ?: emptyList()
                    //Limpiamos la dogimages que es una mutable list de string de imagenes
                    dogImages.clear()
                    //Añadimos las imagenes
                    dogImages.addAll(images)
                    //Recargamos las imagenes en el recycler view
                    adapter.notifyDataSetChanged()
                } else {
                    //muestra error
                    showError()
                }
            }

        }
    }
    //Inicia el recicleview
    private fun initRecyclerView() {
        adapter = DogAdapter(dogImages)
        binding.rvDogs.layoutManager = LinearLayoutManager(this)
        binding.rvDogs.adapter = adapter
    }

    //Método de error usamos toast
    private fun showError() {
        Toast.makeText(this, "Ha ocurrido un error", Toast.LENGTH_SHORT).show()
    }
    //Nos avisa de cada caracter que se añada al buscador
    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }
    //Se obtiene el texto cuando el usuario pulse enter y se lo pasaremos a retrofit para que lo busque en internet
    override fun onQueryTextSubmit(query: String?): Boolean {
        if(!query.isNullOrEmpty()){
            searchByName(query.lowercase())
        }
        return true
    }

}