package com.vicsancab.listdog

import com.google.gson.annotations.SerializedName

//La palabra reservada data convierte el JSON en una entidad (objeto)
data class DogsResponse (
    //SerializedName, esta anotación nos hará de «puente» entre el nombre obligatorio y el que queramos ponerle.
    @SerializedName("status") var status: String,
    @SerializedName("message") var images: List<String>
    )
