package com.vicsancab.listdog

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.vicsancab.listdog.databinding.ItemDogBinding

//Tenemos una función bind y se le pasa una url en formato string usando la librería Picasso
// para cargar la url en nuestro ivDog (layout)
class DogViewHolder(view: View): RecyclerView.ViewHolder(view) {
    private val binding = ItemDogBinding.bind(view)
    fun bind(image:String){
        Picasso.get().load(image).into(binding.ivDog)
    }
}