package com.vicsancab.listdog

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface APIService {
    //Trabaja en tipo get
    @GET
    //suspend para poder trabajar con corrutinas.
    //La anotación url hace referencia a la parte mutable de la dirección de la api y es una string de tipo url.
    //La información proveniente de Retrofit lo capturamos con Response Response<Objeto>
    suspend fun getDogsByBreeds(@Url url:String): Response<DogsResponse>
}